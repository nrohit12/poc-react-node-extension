The following POC contains 3 apps.

Node App : For running api tasks, chatgpt implementation and storing user data

React App: For Signing up the user, getting fine tuning resume and taking interviews

Chrome Extension: For test apply button, auto fill values, login and screen scraping and parsers.

Steps to run the complete app:

1. Open developer mode in Chrome > Click on Load upacked > Select "scraping" folder from the destop
2. Run the React App > Go to "react" folder of the project > Run "npm install" > Run "npm start"
3. Step 1 & 2 will ensure that your frontend part of the project is running.
4. Add your own OPENAI_API_KEY in .env of Node app.
5. Run the Node App > Go to "node" folder of the project > Run "npm install" > Run "npm start"


Your complete project is up and running.

To see the completion of the project:

1. Go to any Linkedin Page > Click on Browser extesion "Web Scraping" > Click on Login Button
2. Step 1 will take you to "localhost:3000/sign-up" > Create user account. 
    You should see a successful message after doing above.
3. Once Signed In/ Logged in > Go to Home Page of your "localhost:3000" or reload "localhost:3000"
4. Now go back to the Linkedin Page > Click on "Test Apply" from bottom left of the page
5. Step 4 takes you to a web page with Application form where all the UI elements will be filled automatically.
6. Now go back to the Linkedin Page > Click on Browser extesion "Web Scraping" > Click on Generate Resume
7. Step 6 will take you to the web page "http://localhost:3000/resume-maker/:id" > Input your resume > You will see a generated fine tuned resume according to the description of the Linkedin Page.
8. Now go back to "http://localhost:3000/" > Click on AI Interview
9. Step 8 will load all the Job Posts on which you have fine tuned your resumes/cv
10. Click on any one Job Post > Take your interview question by question and you will see the ratings after your inputs.

Features of Chrome Extension:

1. Check if you are Loggedin in React App else get you to Sign Up Page
2. Add "Test Apply" Button on Linkedin Page
3. Auto Fill Form on Clicking Test Apply with you updated profile details
4. Get Job Description from Linkedin Page and Fine Tune Resumes

Features of React App:

The App contains following pages:
    a. Sign Up
    b. Login
    c. Resume Fine Tune/ CV generator
    d. All Jobs generated CVs and Job Descriptions
    e. AI interview between Chat GPT and Candidate

1. Interface and functionality for Login/Sign Up
2. Upload your Resume to api > Get User resume > Fine tune their resumes/generate CVs based on Job Description from Linkedin Page
3. Get all of your generated CVs based on Linked Pages you have viewed previously
4. Take Interview on any on generated CVs and view ratings of Each Question.

Features of Node App:

The App contains following apis:
    a. /auth/register > For registering a user
    b. /auth/login > For logging in a registered user
    c. /auth/user-profile/:id > For getting a registered user's profile
    d. /job/create/:id" > For creating a Job Description of a User
    e. /job/job-desc/:id > For getting the generated Job Description of a User
    f. /job/user-job-desc/:id > Get all Jobs generated CVs of a User
    g. /job/fine-tune-resume/:id > For Generating a Fine Tuned Resume or CV based on a Job Description.
    h. /job/generate-interview-questions/:id > For Generating 10 interview questions in JSON from Chat Gpt according to the generated CV and Job description and storing them for reuse
    i./job/rate-interview-question/:id > For Submitting Candidate's response, on a generated question from Chat Gpt and getting the rating from Chat Gpt.




