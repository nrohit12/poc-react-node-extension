const mongooseConnect = require("mongoose");
require("dotenv").config();
const app = require("./app/routes");
mongooseConnect.Promise = global.Promise;
mongooseConnect
  .connect(process.env.MONGO_DB_LOCAL)
  .then((e) => {
    app.listen(process.env.PORT, () => {
      console.log(`Server is running on port ${process.env.PORT}`);
    });
  })
  .catch((e) => console.log(e, "Database connection error."));
