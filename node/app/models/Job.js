const mongoose = require("mongoose");

const JobDescriptionSchema = new mongoose.Schema(
  {
    user: { type: mongoose.Schema.Types.ObjectId, ref: "User", required: true },
    job_description: { type: String, required: true },
  },
  { timestamps: true }
);

const JobUserResumeFineTunedSchema = new mongoose.Schema(
  {
    job_description: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "JobDescription",
      required: true,
    },
    user: { type: mongoose.Schema.Types.ObjectId, ref: "User", required: true },
    fine_tuned: { type: String, required: true },
    rating: { type: Number, required: true },

  },
  { timestamps: true }
);

const JobUserQuestionsSchema = new mongoose.Schema(
  {
    job_description: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "JobDescription",
      required: true,
    },
    user: { type: mongoose.Schema.Types.ObjectId, ref: "User", required: true },
    detail: { type: String, required: true },
    candidate_response: { type: String, required: false },
    candidate_video_response: { type: String, required: false },
    transcription: { type: String, required: false },
    rating: { type: Number, required: false },
    ai_response_transcription: { type: String, required: false },
    ai_response: { type: String, required: false },
  },
  { timestamps: true }
);

const KitUserQuestionsSchema = new mongoose.Schema(
  {
    interview_kit: { type: String, required: true },
    user: { type: mongoose.Schema.Types.ObjectId, ref: "User", required: true },
    question: { type: String, required: true },
    asset_id: { type: String, required: false },
    playback_id: { type: String, required: false },
    rating: { type: Number, required: false },
    time_in_minutes: { type: Number, required: false },
    topic: { type: String, required: true },
    skill: { type: String, required: false },
    transcription: { type: String, required: false },
  },
  { timestamps: true }
);

const JobUserResumeFineTuned = mongoose.model(
  "JobUserResumeFineTuned",
  JobUserResumeFineTunedSchema
);
const JobUserQuestions = mongoose.model(
  "JobUserQuestions",
  JobUserQuestionsSchema
);
const KitUserQuestions = mongoose.model(
  "KitUserQuestions",
  KitUserQuestionsSchema
);
const JobDescription = mongoose.model("JobDescription", JobDescriptionSchema);
module.exports = { JobUserResumeFineTuned, JobDescription, JobUserQuestions, KitUserQuestions };
