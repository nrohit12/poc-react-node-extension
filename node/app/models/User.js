const mongoose = require("mongoose");
const bcrypt = require("bcrypt");

const UserSchema = new mongoose.Schema(
  {
    first_name: { type: String, required: true, unique: true, immutable: true },
    last_name: { type: String, required: true },
    email: { type: String, required: false, unique: true },
    phone: {
      type: String,
      required: true,
      unique: true,
      immutable: true,
    },
    password: { type: String, required: true },
    resume:{ type: String, required: true },
  },
  { timestamps: true }
);

// Hashing password before saving
UserSchema.pre("save", async function (next) {
  if (this.isModified("password")) {
    this.password = await bcrypt.hash(this.password, 10);
  }
  next();
});

UserSchema.methods.toJSON = function () {
  const user = this.toObject();
  delete user.password;
  delete user.username;
  return user;
};

module.exports = mongoose.model("User", UserSchema);
