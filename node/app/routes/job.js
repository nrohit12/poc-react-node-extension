const express = require("express");
const { jobController } = require("../controllers/job");
const jobRouter = express.Router();
const multer = require("multer");
const path = require("path");

const storage = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, "./uploads");
  },
  filename: function (req, file, cb) {
    cb(null, Date.now() + path.extname(file.originalname));
  },
});

const upload = multer({
  storage: storage,
  limits: { fileSize: 25 * 1024 * 1024 },
});
jobRouter.post("/create/:id", jobController.createJobDesc);
jobRouter.get("/job-desc/:id", jobController.getJobDesc);
jobRouter.get("/user-job-desc/:id", jobController.getAllJobDesc);
jobRouter.get("/fine-tune-resume/:id", jobController.fineTuneResume);
jobRouter.get("/generate-interview-questions/:id", jobController.jobInterview);
jobRouter.post(
  "/rate-interview-question/:id",
  jobController.saveCandidateResponse
);
jobRouter.post(
  "/generate-interview-kit-questions/:id",
  jobController.kitInterviewQues
);
jobRouter.post(
  "/rate-interview-kit-question/:id",
  jobController.kitInterviewQuesResponse
);
jobRouter.post(
  "/rate-video-interview-question/:id",
  upload.single("file"),
  jobController.rateVideoQuesResponse
);
jobRouter.post(
  "/convert-file-destination",
  jobController.getAudioVideo
);
module.exports = jobRouter;
