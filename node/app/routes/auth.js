const express = require("express");
const { authController } = require("../controllers/auth");
const authRouter = express.Router();
const multer = require("multer");
const path = require("path");

const storage = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, "./uploads");
  },
  filename: function (req, file, cb) {
    cb(null, Date.now() + path.extname(file.originalname));
  },
});

const upload = multer({
    storage: storage,
    limits: { fileSize: 100 * 1024 * 1024 },
  })

authRouter.post("/register", upload.single("file"), authController.register);
authRouter.post("/login", authController.login);
authRouter.get("/user-profile/:id", authController.userProfile);
module.exports = authRouter;
