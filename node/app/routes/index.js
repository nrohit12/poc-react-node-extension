const express = require("express");
const app = express();
const cors = require("cors");
const bodyParser = require("body-parser");
const { corsOptions } = require("../_utils/corsOptions");

app.use(express.json());
app.use(cors(corsOptions));
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

const authRouter = require("./auth");
const jobRouter = require("./job");
app.use("/v1/auth", authRouter);
app.use("/v1/job", jobRouter);
module.exports = app;