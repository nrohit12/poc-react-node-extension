require("dotenv").config();
const {
  JobDescription,
  JobUserResumeFineTuned,
  JobUserQuestions,
  KitUserQuestions,
} = require("../models/Job");
const OpenAI = require("openai");
const pdfJs = require("pdfjs-dist/legacy/build/pdf");
const ffmpegPath = require("@ffmpeg-installer/ffmpeg").path;
const ffmpeg = require("fluent-ffmpeg");
ffmpeg.setFfmpegPath(ffmpegPath);
const fs = require("fs");
const path = require("path");
const {
  muxVideoMp4Support,
  downloadMp4File,
} = require("../_services/muxVideoMp4Support");

const openai = new OpenAI({
  apiKey: process.env["OPENAI_API_KEY"],
  dangerouslyAllowBrowser: true,
});

const jobController = {
  //sign up
  createJobDesc: async (req, res) => {
    try {
      const { id } = req.params;
      const { job_description } = req.body;

      const jobDesc = new JobDescription({
        job_description,
        user: id,
      });
      const savedJobDec = await jobDesc.save();

      if (!savedJobDec) {
        return res
          .status(500)
          .json({ error: "Unable to save create description." });
      }
      return res.status(201).json({
        message: "Job description created successfully.",
        data: {
          jobDesc: savedJobDec,
        },
      });
    } catch (error) {
      res.status(500).json({ error: error.message || "Internal server error" });
    }
  },

  getJobDesc: async (req, res) => {
    try {
      const { id } = req.params;
      const jobDesc = await JobDescription.findById(id);

      if (!jobDesc) {
        return res
          .status(401)
          .json({ error: "Authentication failed. jobDesc not found." });
      }
      return res.status(200).json({
        message: "jobDesc login successful.",
        data: { jobDesc: jobDesc },
      });
    } catch (error) {
      res.status(500).json({ error: error.message || "Internal server error" });
    }
  },

  getAllJobDesc: async (req, res) => {
    try {
      const { id } = req.params;
      const jobDesc = await JobUserResumeFineTuned.find({ user: id }).populate(
        "job_description"
      );

      if (!jobDesc) {
        return res
          .status(401)
          .json({ error: "Authentication failed. User not found." });
      }
      return res.status(200).json({
        message: "jobDesc get successful.",
        data: { jobDesc: jobDesc },
      });
    } catch (error) {
      res.status(500).json({ error: error.message || "Internal server error" });
    }
  },
  fineTuneResume: async (req, res) => {
    try {
      const { id } = req.params;
      const jobDesc = await JobDescription.findById(id).populate(
        "job_description user"
      );
      if (!jobDesc) {
        return res
          .status(400)
          .json({ error: "Job Description does not exists" });
      }

      const jobDescFineTuneExists = await JobUserResumeFineTuned.findOne({
        job_description: id,
      }).populate("job_description user");
      if (jobDescFineTuneExists) {
        return res.status(200).json({
          message: "Resume Fine Tuned successfully.",
          data: {
            generated_response: jobDescFineTuneExists,
          },
        });
      }
      const file = fs.readFileSync(jobDesc.user.resume);
      const buffer = file.buffer;
      const pdfData = new Uint8Array(buffer);
      const loadingTask = pdfJs.getDocument(pdfData);
      const pdfDocument = await loadingTask.promise;
      let textContent = "";
      for (let pageNum = 1; pageNum <= pdfDocument.numPages; pageNum++) {
        const page = await pdfDocument.getPage(pageNum);
        const pageText = await page.getTextContent();
        const pageContent = pageText.items.map((item) => item.str).join(" ");
        textContent += pageContent;
      }

      if (textContent.length && jobDesc.job_description) {
        const completion = await openai.chat.completions.create({
          messages: [
            {
              role: "system",
              content: `You are a helpful assistant. Fine tune the resume given below according to job description : ${jobDesc.job_description} and rate it out of 5. Generate the output in json format {fine_tuned, rating}`,
            },
            {
              role: "user",
              content: `{Fine tune my resume: ${textContent} }`,
            },
          ],
          model: "gpt-3.5-turbo-1106",
          response_format: { type: "json_object" },
        });
        const chatGptRes = JSON.parse(completion.choices[0].message.content);
        const jobDescFineTune = new JobUserResumeFineTuned({
          job_description: jobDesc._id,
          user: jobDesc.user._id,
          fine_tuned: JSON.stringify(chatGptRes.fine_tuned),
          rating: chatGptRes.rating,
        });
        await jobDescFineTune.save();
        return res.status(200).json({
          message: "Resume Fine Tuned successfully.",
          data: {
            generated_response: jobDescFineTune,
          },
        });
      }
    } catch (error) {
      res.status(500).json({ error: error.message || "Internal server error" });
    }
  },
  jobInterview: async (req, res) => {
    try {
      const { id } = req.params;

      const jobDesc = await JobUserResumeFineTuned.findById(id).populate(
        "job_description"
      );

      if (!jobDesc) {
        return res
          .status(400)
          .json({ error: "Job Description does not exists" });
      }

      const jobSavedQuestions = await JobUserQuestions.find({
        job_description: jobDesc.job_description._id,
      });

      if (jobSavedQuestions.length > 0) {
        return res.status(200).json({
          message: "Questions fetched successfully.",
          data: {
            questions: jobSavedQuestions,
          },
        });
      }

      const completion = await openai.chat.completions.create({
        messages: [
          {
            role: "user",
            content: `{Create 10 Interview questions in json format {questions} for Job: ${jobDesc.job_description.job_description} - according to the resume : ${jobDesc.response} }`,
          },
        ],
        model: "gpt-3.5-turbo-1106",
        response_format: { type: "json_object" },
      });
      const chatGptRes = JSON.parse(completion.choices[0].message.content);
      if (chatGptRes?.questions) {
        let quesObj = [];
        for (let v in chatGptRes.questions) {
          quesObj.push({
            job_description: jobDesc.job_description._id,
            user: jobDesc.user,
            detail: chatGptRes.questions[v].question,
          });
        }
        let savedQuestions = await JobUserQuestions.create(quesObj);
        return res.status(200).json({
          message: "Questions fetched successfully.",
          data: {
            questions: savedQuestions,
          },
        });
      }
    } catch (error) {
      res.status(500).json({ error: error.message || "Internal server error" });
    }
  },

  saveCandidateResponse: async (req, res) => {
    try {
      const { id } = req.params;
      const { candidate_response } = req.body;

      const jobQues = await JobUserQuestions.findById(id);

      if (!jobQues) {
        return res.status(400).json({ error: "Job Question does not exists" });
      }

      const completion = await openai.chat.completions.create({
        messages: [
          {
            role: "user",
            content: `{Rate candidate response: ${candidate_response} out of 5 in json format {rating} for the question: ${jobQues.detail} }`,
          },
        ],
        model: "gpt-3.5-turbo-1106",
        response_format: { type: "json_object" },
      });
      const chatGptRes = JSON.parse(completion.choices[0].message.content);

      jobQues["rating"] = chatGptRes.rating;
      jobQues["candidate_response"] = candidate_response;

      await jobQues.save();

      return res.status(200).json({
        message: "Job Question saved successfully.",
        data: {
          rating: jobQues,
        },
      });
    } catch (error) {
      res.status(500).json({ error: error.message || "Internal server error" });
    }
  },
  kitInterviewQues: async (req, res) => {
    try {
      const { interview_kit, skills, topics } = req.body;
      const { id } = req.params;
      const kitInterviewQues = await KitUserQuestions.find({ interview_kit });

      if (kitInterviewQues.length > 0) {
        return res.status(200).json({
          message: "Questions fetched successfully.",
          data: {
            questions: kitInterviewQues,
          },
        });
      }

      const completion = await openai.chat.completions.create({
        messages: [
          {
            role: "user",
            content: `{Create 10 assessment questions in json format {question,time_in_minutes,topic,skill} for topics: ${topics} - and skills : ${skills} }`,
          },
        ],
        model: "gpt-3.5-turbo-1106",
        response_format: { type: "json_object" },
      });
      const chatGptRes = JSON.parse(completion.choices[0].message.content);
      if (chatGptRes?.questions) {
        let quesObj = [];
        for (let v in chatGptRes.questions) {
          quesObj.push({
            interview_kit,
            user: id,
            question: chatGptRes.questions[v].question,
            time_in_minutes: chatGptRes.questions[v].time_in_minutes,
            topic: chatGptRes.questions[v].topic,
            skill: chatGptRes.questions[v].skill,
          });
        }
        let savedQuestions = await KitUserQuestions.create(quesObj);
        return res.status(200).json({
          message: "Questions fetched successfully.",
          data: {
            questions: savedQuestions,
          },
        });
      }
    } catch (error) {
      res.status(500).json({ error: error.message || "Internal server error" });
    }
  },
  rateVideoQuesResponse: async (req, res) => {
    try {
      const { id } = req.params;

      const jobQues = await JobUserQuestions.findById(id);
      if (!jobQues) {
        return res
          .status(400)
          .json({ error: "Assessment Kit Question does not exists" });
      }
      ffmpeg(req.file.path)
        .noVideo()
        .audioCodec("libmp3lame")
        .on("end", () => {
          console.log("Conversion finished!");
        })
        .on("error", (err) => {
          console.error("Error:", err);
        })
        .save(`./uploads/${id}.mp3`);

      await setTimeout(async () => {
        const transcription = await openai.audio.transcriptions.create({
          file: fs.createReadStream(`./uploads/${id}.mp3`),
          model: "whisper-1",
        });
        if (transcription?.text) {
          jobQues["transcription"] = transcription?.text;
          jobQues["candidate_video_response"] = req.file.path;
          const completion = await openai.chat.completions.create({
            messages: [
              {
                role: "system",
                content: `You are a helpful assisstant. You need to rate the response below out of 5 in json format {rating, feedback} for the for the question: ${jobQues.detail}`,
              },
              {
                role: "user",
                content: `{Rate the response: ${transcription?.text} }`,
              },
            ],
            model: "gpt-3.5-turbo-1106",
            response_format: { type: "json_object" },
          });
          const chatGptRes = JSON.parse(completion.choices[0].message.content);

          jobQues["rating"] = chatGptRes?.rating;
          jobQues["ai_response_transcription"] = chatGptRes?.feedback;
          const speechFile = path.resolve(`./uploads/${id}.mp3`);

          const mp3 = await openai.audio.speech.create({
            model: "tts-1",
            voice: "alloy",
            input: `${chatGptRes?.feedback}`,
          });
          const buffer = Buffer.from(await mp3.arrayBuffer());
          await fs.promises.writeFile(speechFile, buffer);
        }

        jobQues["ai_response"] = `uploads/${id}.mp3`;

        await jobQues.save();

        console.log("jfnekahfaefa =>>>>>>>>>>>>>>> response saved");

        return res.status(200).json({
          message: "Job Question saved successfully.",
          data: {
            jobQues,
          },
        });
      }, 10000);
    } catch (error) {
      console.log(error);
      res.status(500).json({ error: error.message || "Internal server error" });
    }
  },
  kitInterviewQuesResponse: async (req, res) => {
    try {
      const { id } = req.params;
      const { asset_id } = req.body;

      const kitQues = await KitUserQuestions.findById(id);
      if (!kitQues) {
        return res
          .status(400)
          .json({ error: "Assessment Kit Question does not exists" });
      }
      const data = await muxVideoMp4Support(asset_id, "standard");
      if (!data) {
        return res.status(404).json({
          message: "Unable to retrieve video url.",
        });
      }

      const playback_id = data.playback_ids[0].id;

      await setTimeout(async () => {
        await downloadMp4File(playback_id);
        const transcription = await openai.audio.transcriptions.create({
          file: fs.createReadStream(`./uploads/${playback_id}.mp4`),
          model: "whisper-1",
        });

        if (transcription?.text) {
          kitQues["asset_id"] = asset_id;
          kitQues["playback_id"] = playback_id;
          kitQues["transcription"] = transcription?.text;
          await kitQues.save();

          const completion = await openai.chat.completions.create({
            messages: [
              {
                role: "system",
                content: `You are a helpful assisstant. You need to rate the response below out of 5 in json format {rating} for the for the question: ${kitQues.question} according to the skill: ${kitQues.skill}`,
              },
              {
                role: "user",
                content: `{Rate the response: ${transcription?.text} }`,
              },
            ],
            model: "gpt-3.5-turbo-1106",
            response_format: { type: "json_object" },
          });
          const chatGptRes = JSON.parse(completion.choices[0].message.content);
          kitQues["rating"] = chatGptRes.rating;
          await kitQues.save();
        }
        await muxVideoMp4Support(asset_id, "none");
      }, 20000);

      return res.status(200).json({
        message: "Job Question saved successfully.",
        data: {
          kitQues,
        },
      });
    } catch (error) {
      console.log(error);
      res.status(500).json({ error: error.message || "Internal server error" });
    }
  },

  getAudioVideo: async (req, res) => {
    try {
      const { audio, video } = req.body;
      const destinationPath =
        "/Users/deadblss/Documents/poc-react-node-extension/react/public/";
      await fs.readFile(audio, (err, data) => {
        if (err) {
          console.error("Error reading source file:", err);
          return;
        }
        fs.writeFile(`${destinationPath}${audio}`, data, (err) => {
          if (err) {
            console.error("Error writing to destination file:", err);
            return;
          }
        });
      });
      await fs.readFile(video, (err, data) => {
        if (err) {
          console.error("Error reading source file:", err);
          return;
        }
        fs.writeFile(`${destinationPath}${video}`, data, (err) => {
          if (err) {
            console.error("Error writing to destination file:", err);
            return;
          }
        });
      });

      return res.status(200).json({
        message: "Job Question saved successfully.",
        data: {
          audio,
          video,
        },
      });
    } catch (error) {
      console.log(error);
      res.status(500).json({ error: error.message || "Internal server error" });
    }
  },
};
// TO FE
// const callChatGpt = async () => {
//   const file = "https://stream.mux.com/3QiMF1iX1cheLqPUTvP9R2AK6lfj15mXkEqjyIwQXCg/low.mp4?download=cats"
//   const transcription = await openai.audio.transcriptions.create({
//     file: fs.createReadStream('./uploads/1704101541057.webm'),
//     model: "whisper-1",
//   });
//   console.log(transcription?.text);
// };

// callChatGpt();

module.exports = { jobController };
