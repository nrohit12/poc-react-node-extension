const User = require("../models/User");
const bcrypt = require("bcrypt");
const authController = {
  //sign up
  register: async (req, res) => {
    try {
      const { first_name, last_name, email, phone, password } = req.body;

      const existingUser = await User.findOne({
        $or: [{ email }, { phone }],
      });
      if (existingUser) {
        return res.status(400).json({ error: "Email or phone already exists" });
      }
      const user = new User({
        first_name,
        last_name,
        email,
        phone,
        password,
        resume: req.file.path, 
      });
      const savedUser = await user.save();

      if (!savedUser) {
        return res.status(500).json({ error: "Unable to create user." });
      }
      return res.status(201).json({
        message: "User created successfully.",
        data: {
          user: savedUser,
        },
      });
    } catch (error) {
      res.status(500).json({ error: error.message || "Internal server error" });
    }
  },

  login: async (req, res) => {
    try {
      const { email, password } = req.body;
      const user = await User.findOne({ email });
      if (!user) {
        return res
          .status(401)
          .json({ error: "Authentication failed. User not found." });
      }

      bcrypt.compare(password, user.password, (error, result) => {
        if (error) {
          return res.status(401).json({
            error: "Authentication failed. Password does not match.",
          });
        }
        // login and return user
        if (result) {
          return res.status(200).json({
            message: "User login successful.",
            data: { user: user },
          });
        }
      });
    } catch (error) {
      res.status(500).json({ error: error.message || "Internal server error" });
    }
  },

  userProfile: async (req, res) => {
    try {
      const { id } = req.params;

      const existingUser = await User.findById(id);

      if (!existingUser) {
        return res.status(400).json({ error: "User does not exists" });
      }

      return res.status(200).json({
        message: "User fetched successfully.",
        data: {
          user: existingUser,
        },
      });
    } catch (error) {
      res.status(500).json({ error: error.message || "Internal server error" });
    }
  },
};

module.exports = { authController };
