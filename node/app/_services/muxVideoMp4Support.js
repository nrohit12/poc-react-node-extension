const axios = require("axios");
const fs = require("fs");
require("dotenv").config();

const muxVideoMp4Support = async (assetId, support) => {
  
  try {
    const response = await axios({
      async: true,
      crossDomain: true,
      url: `https://api.mux.com/video/v1/assets/${assetId}/mp4-support`,
      method: "PUT",
      headers: {
        Authorization: `Basic ${Buffer.from(
          `${process.env.MUX_TOKEN_ID}:${process.env.MUX_TOKEN_SECRET}`
        ).toString("base64")}`,
        "Content-Type": "application/json",
      },
      data: { mp4_support: support },
    });
    if (response.status) {
      let data = response.data;
      return data.data;
    }
  } catch (e) {
    return false;
  }
};

const downloadMp4File = async (id) => {
  try {
    const response = await axios({
      method: 'GET',
      url: `https://stream.mux.com/${id}/low.mp4?download=${id}`,
      responseType: 'stream',
    });

    const writer = fs.createWriteStream(`./uploads/${id}.mp4`);

    response.data.pipe(writer);

    return new Promise((resolve, reject) => {
      writer.on('finish', resolve);
      writer.on('error', reject);
    });
  } catch (error) {
    console.log(error)
    throw new Error(`Error downloading file: ${error}`);
  }
};

module.exports = { muxVideoMp4Support, downloadMp4File };
