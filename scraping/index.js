// automatically get the link not working

// var myButton = document.querySelector(".jobs-apply-button");
// console.log(myButton.attributes); -> Unable to get link from here

(function () {
  if (window.location.hostname === "www.linkedin.com") {
    const button = document.createElement("button");
    button.innerText = "Test Apply";
    button.id = "testApplyButton";
    button.href =
      "https://boards.greenhouse.io/integrateai/jobs/7069900002?gh_jid=7069900002&source=LinkedIn";

    button.addEventListener("click", function () {
      window.open(this.href, "_blank");
    });
    document.body.appendChild(button);
  }
})();

//if test-node-user is there set it in chrome extension storage
(function () {
  if (window.location.hostname === "localhost") {
    const storedUser = localStorage.getItem("test-node-user");
    if (storedUser) {
      chrome.storage.local.set({ testNodeUser: storedUser }, function () {
        console.log("Success");
      });
    }
  }
})();

// const testApplyButton = document.getElementById("scrapingButtonTestApply")
// testApplyButton.href =
//       "https://boards.greenhouse.io/integrateai/jobs/7069900002?gh_jid=7069900002&source=LinkedIn";
// testApplyButton.addEventListener("click", function () {
//   window.open(this.href, "_blank");
// });

//login and fill values
(function () {
  document.addEventListener("DOMContentLoaded", function () {
    chrome.storage.local.get(["testNodeUser"], function (result) {
      let user = result?.testNodeUser;
      const loginForm = document.getElementById("formLoginButton");
      const interviewButton = document.getElementById(
        "scrapingButtonInterview"
      );
      const resumeButton = document.getElementById(
        "scrapingButtonGenerateResponse"
      );

      if (user) {
        loginForm.classList.add("hidden");
        resumeButton.classList.remove("hidden");
        interviewButton.classList.remove("hidden");
      } else {
        loginForm.classList.remove("hidden");
        resumeButton.classList.add("hidden");
        interviewButton.classList.add("hidden");
      }
    });
  });
})();

(function () {
  chrome.storage.local.get(["testNodeUser"], function (result) {
    const formElement = document.getElementsByTagName("form");
    let user = result.testNodeUser;
    if (formElement.length) {
      if (user) {
        let parsedUser = JSON.parse(user);
        const apiUrl = `http://localhost:4000/v1/auth/user-profile/${parsedUser.user._id}`;
        fetch(apiUrl)
          .then((response) => response.json())
          .then((data) => {
            let userData = data.data.user;
            for (var item in userData) {
              let input = document.getElementById(item);
              if (input) input.setAttribute("value", userData[item]);
            }
            alert("User data filled.");
          })
          .catch((error) => console.error("Error:", error));
      }
    }
  });
})();

(function () {
  const formLogin = document.getElementById("formLoginButton");
  formLogin &&
    formLogin.addEventListener("click", function () {
      window.open("http://localhost:3000/sign-up", "_blank");
    });
})();

(function () {
  setTimeout(() => {
    var elements = document.querySelector(".jobs-description__content");
    if (elements?.textContent) {
      chrome.storage.local.get(["testNodeUser"], function (result) {
        let user = result.testNodeUser;
        if (user) {
          let parsedUser = JSON.parse(user);
          const apiUrl = `http://localhost:4000/v1/job/create/${parsedUser.user._id}`;
          fetch(apiUrl, {
            method: "POST",
            headers: {
              "Content-Type": "application/json",
            },
            body: JSON.stringify({ job_description: elements.textContent }),
          })
            .then((response) => response.json())
            .then((data) => {
              chrome.storage.local.set(
                { jobDescID: data.data.jobDesc },
                function () {
                  console.log("Success");
                }
              );
            })
            .catch((error) => console.error("Error:", error));
        }
      });
    }
  }, 2000);
})();

chrome.storage.local.get(["jobDescID"], function (result) {
  let jobData = result.jobDescID;
  const formGenerateResponse = document.getElementById(
    "scrapingButtonGenerateResponse"
  );
  formGenerateResponse &&
    formGenerateResponse.addEventListener("click", function () {
      if (jobData) {
        window.open(
          `http://localhost:3000/resume-maker/${jobData._id}`,
          "_blank"
        );
      }
    });
});

(function () {
  setTimeout(() => {
    const formInterview = document.getElementById("scrapingButtonInterview");
    formInterview &&
      formInterview.addEventListener("click", function () {
        window.open(`http://localhost:3000/ai-interview`, "_blank");
      });
  }, 1000);
})();
