import { BrowserRouter, Routes, Route } from "react-router-dom";

import Home from "./pages/Home";
import SignUp from "./pages/SignUp";
import ResumeMaker from "./pages/ResumeMaker";
import TextInterview from "./pages/TextInterview";
import Login from "./pages/Login";
import AllJobDesc from "./pages/AllJobsDesc";
import AssessmetKit from "./pages/AssessmentKit";
import VideInterview from "./pages/VideoInterview";

function App() {
  return (
    <BrowserRouter>
      <Routes>
        <Route index element={<Home />} />
        <Route path="/sign-up" element={<SignUp />} />
        <Route path="/login" element={<Login />} />
        <Route path="/resume-maker/:id" element={<ResumeMaker />} />
        <Route path="/ai-interview" element={<AllJobDesc />} />
        <Route path="/text-interview/:id" element={<TextInterview />} />
        <Route path="/video-interview/:id" element={<VideInterview />} />
        <Route path="/assessment-kit/:id" element={<AssessmetKit />} />
      </Routes>
    </BrowserRouter>
  );
}

export default App;
