import axios from "axios";
const axiosInstance = axios.create({
    baseURL: "http://localhost:4000/v1"
});

axiosInstance.interceptors.response.use(
    (response) => {
        if (response.status === 401) {
            alert("You are not authorized");
        }
        return response;
    },
    (error) => {
        if (
            error.response &&
            error.response.data &&
            error.response.data.status === 401
        ) {
            localStorage.removeItem("jigsawUser");
            window.location.reload();
            return Promise.reject(error.response && error.response.data);
        }
        return Promise.reject(error.response.data);
    }
);
export default axiosInstance;