import React, { useEffect, useState } from "react";
import axiosInstance from "../../_config";
import { useNavigate } from "react-router-dom";

function AllJobDesc() {
  const [generatedUserJobDesc, setGeneratedUserJobDesc] = useState([]);
  const [loading, setLoading] = useState(false);

  const navigate = useNavigate();

  const [user, setUser] = useState();

  useEffect(() => {
    const signIn = localStorage.getItem("test-node-user");
    setUser(JSON.parse(signIn));
  }, []);

  useEffect(() => {
    loadData();
  }, [user]);

  const loadData = async (e) => {
    setLoading(true);
    if (user) {
      const res = await axiosInstance.get(
        `/job/user-job-desc/${user.user._id}`
      );
      if (res.data) {
        const result = res.data.data;

        setGeneratedUserJobDesc(result.jobDesc);
      }
    }
    setLoading(false);
  };

  const onInterviewStartClick = async (id, name) => {
    localStorage.removeItem("test-node-user-rating");
    setLoading(true);
    if (id) {
      navigate(`/${name}-interview/${id}`);
    }
    setLoading(false);
  };

  return (
    <div>
      <div>
        {" "}
        {loading ? (
          <h4>Data Loading......</h4>
        ) : (
          <div>
            {Array.isArray(generatedUserJobDesc) &&
              generatedUserJobDesc.map((item, index) => (
                <div>
                  <h3>
                    {index + 1}. <span>Rating: {item?.rating}</span>
                  </h3>

                  <h6>
                    {item?.job_description &&
                      item.job_description?.job_description}
                  </h6>
                  <p>
                    Fine Tuned Resume: <span>{item?.fine_tuned}</span>
                  </p>
                  <button onClick={() => onInterviewStartClick(item._id, "text")}>
                    Start Text Interview
                  </button>
                  <button onClick={() => onInterviewStartClick(item._id,"video")}>
                    Start Video Interview
                  </button>
                </div>
              ))}
          </div>
        )}
      </div>
    </div>
  );
}

export default AllJobDesc;
