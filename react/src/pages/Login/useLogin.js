import { useState } from "react";
import { useNavigate } from "react-router-dom";
import axiosInstance from "../../_config";

const formSchema = {
  email: "",
  password: "",
};

function useLogin() {
  const [showPassword, setShowPassword] = useState(false);
  const [formData, setFormData] = useState({});

  const navigate = useNavigate();

  const onSubmitClick = async (e) => {
    e.preventDefault();

    //if form not valid
    if (!validateForm()) {
      alert("Please input all fields");
      return;
    }

    //if form valid

    const res = await axiosInstance.post("/auth/login", formData);

    if (res.data) {
      localStorage.setItem("test-node-user", JSON.stringify(res.data.data));
      alert("Login successful.");
      navigate("/");
    }
  };

  //input change handling
  const onInputChange = (val, name) => {
    let data = formData;
    data[name] = val;
    setFormData(data);
  };
  //view/hide password
  const onPasswordView = () => setShowPassword((val) => !val);

  const validateForm = () => {
    //check is form valid
    let valid = { isValid: true };
    for (var key in formSchema) {
      if (formData[key] === undefined || formData[key].length === 0) {
        valid.isValid = false;
      }
    }
    return valid.isValid;
  };

  return {
    showPassword,
    onSubmitClick,
    onInputChange,
    onPasswordView,
  };
}

export default useLogin;
