import React from "react";
import useLogin from "./useLogin";

function Login() {
  const { showPassword, onSubmitClick, onInputChange, onPasswordView } =
    useLogin();
  return (
    <div className="text-center">
      <form onSubmit={onSubmitClick}>
        <div className="form-group">
          <label htmlFor="Email" className="mb-2 font-weight-medium">
            Email ID
          </label>
          <input
            type="email"
            placeholder="Enter your Email ID"
            onChange={(e) => onInputChange(e.target.value, "email")}
          />
        </div>

        <div className="form-group">
          <label htmlFor="Password" className="mb-2 font-weight-medium">
            Password
          </label>
          <div>
            <input
              type={showPassword ? "text" : "password"}
              placeholder="Enter your new password"
              onChange={(e) => onInputChange(e.target.value, "password")}
            />
            <span onClick={onPasswordView}>View</span>
          </div>
        </div>

        <button className="btn btn-primary ">Login</button>
      </form>

      <a className="btn btn-secondary my-2" href="/">
        Go Home
      </a>
    </div>
  );
}

export default Login;
