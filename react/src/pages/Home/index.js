import React, { useEffect, useState } from "react";

const Home = () => {
  const [user, setUser] = useState();

  useEffect(() => {
    const signIn = localStorage.getItem("test-node-user");
    setUser(JSON.parse(signIn));
  }, []);

  return (
    <div className="text-center">
      <div className="my-2">
        {user ? (
          <div>
            <button className="btn btn-secondary mr-2">
              {user?.user.first_name} {user?.user.last_name}
            </button>
            <div className="my-2 ">
              <a href="/ai-interview" className="btn btn-danger mr-2">
                Go to AI Interview
              </a>
              <a
                href="/assessment-kit/173"
                className="btn btn-danger mr-2"
              >
                Go to Assessment Kit
              </a>
            </div>
          </div>
        ) : (
          <div>
            <a href="/sign-up" className="btn btn-primary mr-2">
              Go to Sign Up
            </a>
            <a href="/login" className="btn btn-primary">
              Go to Login
            </a>
          </div>
        )}
      </div>
    </div>
  );
};

export default Home;
