import { useRef, useState, useEffect } from "react";
import axiosInstance from "../../_config";
import { useParams } from "react-router-dom";
import ReactPlayer from "react-player";

function VideInterview() {
  const stopButtonRef = useRef(null);
  const [blobUrl, setBlobUrl] = useState(null);
  const videoElement = useRef(null);
  const [generatedUserQuestions, setGeneratedUserQuestions] = useState([]);
  const [currentPage, setCurrentPage] = useState(null);
  const [currentRating, setCurrentRating] = useState(null);
  const [loading, setLoading] = useState(false);
  const [saveLoading, setSaveLoading] = useState(false);

  const params = useParams();

  useEffect(() => {
    const loadData = async () => {
      setLoading(true);
      if (params?.id) {
        const res = await axiosInstance.get(
          `/job/generate-interview-questions/${params.id}`
        );
        if (res.data) {
          const result = res.data.data;
          let url = result.questions[0]?.candidate_video_response;
          setGeneratedUserQuestions(result.questions);
          let page = await localStorage.getItem("test-node-user-rating");
          if (page) {
            setCurrentPage(parseInt(page));
          } else setCurrentPage(0);
          setCurrentRating(parseInt(page));

          if (url) setBlobUrl(url);
        }
      }
      setLoading(false);
    };

    loadData();
  }, [params.id]);

  useEffect(() => {
    if (currentPage !== null) {
      setBlobUrl(null);
      startRecording();
    }
  }, [currentPage]);

  function startRecording() {
    const constraints = {
      audio: {
        autoGainControl: false,
        echoCancellation: false,
        noiseSuppression: false,
      },
      video: true,
      echoCancellation: true,
    };
    navigator.mediaDevices.getUserMedia(constraints).then(function (stream) {
      const options = {
        mimeType: "video/webm",
      };
      const recordedChunks = [];
      const mediaRecorder = new MediaRecorder(stream, options);

      if (videoElement?.current) videoElement.current.srcObject = stream;

      mediaRecorder.addEventListener("dataavailable", function (e) {
        if (e.data.size > 0) recordedChunks.push(e.data);
      });

      mediaRecorder.addEventListener("stop", function () {
        setBlobUrl(URL.createObjectURL(new Blob(recordedChunks)));
        stream.getTracks().forEach((track) => track.stop());
        videoObject(URL.createObjectURL(new Blob(recordedChunks)));
      });

      if (stopButtonRef && stopButtonRef.current) {
        stopButtonRef?.current?.addEventListener(
          "click",
          function onStopClick() {
            mediaRecorder.stop();
            this.removeEventListener("click", onStopClick);
            videoElement.current.srcObject = null;
          }
        );
      }

      mediaRecorder.start();
    });
  }

  const videoObject = async (url) => {
    const imageResponse = await fetch(url);
    let data = await imageResponse.blob();
    let metadata = {
      type: "video/webm",
    };
    let uploadFile = new File([data], "file.webm", metadata);

    const formData = new FormData();
    formData.append("file", uploadFile);

    setSaveLoading(true);

    const res = await axiosInstance.post(
      `/job/rate-video-interview-question/${generatedUserQuestions[currentPage]._id}`,
      formData
    );
    if (res.data) {
      setCurrentPage((v) => v + 1);
    }

    setSaveLoading(false);
  };

  const fetchFile = async () => {
    const formData = {
      audio: generatedUserQuestions[currentPage]?.ai_response,
      video: generatedUserQuestions[currentPage]?.candidate_video_response,
    };

    if (formData?.audio && formData?.video) {
      await localStorage.setItem("test-node-user-rating", currentPage);
      const res = await axiosInstance.post(
        `/job/convert-file-destination/`,
        formData
      );
      if (res.data.data) setCurrentRating(res.data.data);
    }
  };

  return (
    <div>
      {loading ? (
        <h4>Data Loading......</h4>
      ) : (
        <div>
          <div>
            <h3>{currentPage + 1}</h3>
            <h6>{generatedUserQuestions[currentPage]?.detail}</h6>

            {generatedUserQuestions[currentPage]?.rating ? (
              <div>
                <p>
                  Candidate is rated :{" "}
                  {generatedUserQuestions[currentPage]?.rating} / 5
                </p>

                {currentRating === currentPage ? (
                  <div>
                    <p>Your Response:</p>
                    <video width="640" height="360" controls>
                      <source
                        src={`/${generatedUserQuestions[currentPage]?.candidate_video_response}`}
                        type="video/webm"
                      />
                      Your browser does not support the video tag.
                    </video>
                    <p>Feedback:</p>
                    <audio controls>
                      <source
                        src={`/${generatedUserQuestions[currentPage]?.ai_response}`}
                        type="audio/mpeg"
                      />
                      Your browser does not support the audio element.
                    </audio>
                  </div>
                ) : (
                  <button className="btn btn-secondary" onClick={fetchFile}>
                    Response saved click to listen feedback and response
                  </button>
                )}

                <button onClick={() => setCurrentPage((v) => v + 1)}>
                  Next Question
                </button>
              </div>
            ) : saveLoading ? (
              <div>
                <button className="btn btn-secondary">
                  Saving your response
                </button>
              </div>
            ) : (
              <div>
                <video id="player" ref={videoElement} autoPlay />
                <button ref={stopButtonRef}>Save Response</button>
              </div>
            )}
          </div>
        </div>
      )}
    </div>
  );
}

export default VideInterview;
