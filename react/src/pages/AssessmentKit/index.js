import React, { useEffect, useState } from "react";
import axiosInstance from "../../_config";
import { useParams } from "react-router-dom";

function AssessmetKit() {
  const [generatedUserJobQuestions, setGeneratedUserJobQuestions] = useState(
    []
  );
  const [currentPage, setCurrentPage] = useState(null);

  const [loading, setLoading] = useState(false);
  const [userInput, setUserInput] = useState("");

  const params = useParams();

  useEffect(() => {
    const loadData = async () => {
      setLoading(true);

      const signIn = localStorage.getItem("test-node-user");
      const parsedUser = JSON.parse(signIn);

      if (parsedUser) {
        const res = await axiosInstance.post(
          `/job/generate-interview-kit-questions/${parsedUser.user._id}`,
          {
            topics: [
              "frontend develop",
              "frontend develop",
              "frontend develop",
              "frontend develop",
              "frontend develop",
              "frontend develop",
              "frontend developer",
            ],
            skills: [
              ["Map Reduce"],
              ["React.js"],
              ["React.js"],
              ["React.js"],
              ["React.js"],
              ["React.js"],
              ["Frontend"],
            ],
            interview_kit: 173,
          }
        );
        if (res.data) {
          const result = res.data.data;

          setGeneratedUserJobQuestions(result.questions);
          setCurrentPage(0);
        }
      }
      setLoading(false);
    };

    loadData();
  }, [params.id]);

  const onSaveResponseClick = async (id) => {
    setLoading(true);

    if (currentPage !== null) {
      const res = await axiosInstance.post(
        `/job/rate-interview-kit-question/${generatedUserJobQuestions[currentPage]._id}`,
        { asset_id: userInput }
      );
      if (res.data) {
        setCurrentPage((v) => v + 1);
      }
    }
    setUserInput(null);

    setLoading(false);
  };

  return (
    <div>
      <div>
        {loading ? (
          <h4>Data Loading......</h4>
        ) : (
          <div>
            <h3>{currentPage + 1}</h3>
            <h6>{generatedUserJobQuestions[currentPage]?.question}</h6>
            {generatedUserJobQuestions[currentPage]?.rating ? (
              <p>
                Your response :{" "}
                {generatedUserJobQuestions[currentPage]?.candidate_response}
              </p>
            ) : (
              <textarea
                value={userInput}
                onChange={(e) => setUserInput(e.target.value)}
              />
            )}
            {generatedUserJobQuestions[currentPage]?.rating ? (
              <div>
                <p>
                  Candidate is rated :{" "}
                  {generatedUserJobQuestions[currentPage]?.rating} / 5
                </p>
                <button onClick={() => setCurrentPage((v) => v + 1)}>
                  Next Question
                </button>
              </div>
            ) : (
              <button onClick={() => onSaveResponseClick()}>
                Save Response
              </button>
            )}
          </div>
        )}
      </div>
    </div>
  );
}

export default AssessmetKit;
