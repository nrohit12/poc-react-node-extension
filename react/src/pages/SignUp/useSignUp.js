import { useState } from "react";
import { useNavigate } from "react-router-dom";
import axiosInstance from "../../_config";

const formSchema = {
  first_name: "",
  last_name: "",
  email: "",
  phone: "",
  password: "",
};

function useSignUp() {
  const [showPassword, setShowPassword] = useState(false);
  const [formData, setFormData] = useState({});
  const [fileInput, setFileInput] = useState(null);

  const navigate = useNavigate();

  const onSubmitClick = async (e) => {
    e.preventDefault();

    let { valid, userFormData } = validateForm();

    if (fileInput === null) {
      alert("Please input resume");
      return;
    }
    userFormData.append("file", fileInput);
    //if form not valid
    if (!valid.isValid) {
      alert("Please input all fields");
      return;
    }

    //if form valid
    const res = await axiosInstance.post("/auth/register", userFormData);

    if (res.data) {
      localStorage.setItem("test-node-user", JSON.stringify(res.data.data));
      alert("Sign up successful.");
      navigate("/");
    }
  };

  //input change handling
  const onInputChange = (val, name) => {
    let data = formData;
    data[name] = val;
    setFormData(data);
  };

  const onFileChange = async (e) => {
    const file = e.target.files[0];
    setFileInput(file);
  };
  //view/hide password
  const onPasswordView = () => setShowPassword((val) => !val);

  const validateForm = () => {
    //check is form valid
    const userFormData = new FormData();
    let valid = { isValid: true };
    for (var key in formSchema) {
      if (
        key !== "resume" &&
        (formData[key] === undefined || formData[key].length === 0)
      ) {
        valid.isValid = false;
      } else {
        userFormData.append(key, formData[key]);
      }
    }
    console.log(userFormData);
    return { valid, userFormData };
  };

  return {
    showPassword,
    onSubmitClick,
    onInputChange,
    onPasswordView,
    onFileChange,
  };
}

export default useSignUp;
