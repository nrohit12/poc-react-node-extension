import React from "react";
import useSignUp from "./useSignUp";

function SignUp() {
  const {
    showPassword,
    onSubmitClick,
    onInputChange,
    onPasswordView,
    onFileChange
  } = useSignUp();
  return (
    <div className="text-center">
      <form onSubmit={onSubmitClick}>
        <div className="form-group">
          <label htmlFor="FirstName" className="mb-2 font-weight-medium">
            First Name
          </label>
          <input
            type="text"
            placeholder="Enter your First Name"
            onChange={(e) => onInputChange(e.target.value, "first_name")}
          />
        </div>
        <div className="form-group">
          <label htmlFor="LastName" className="mb-2 font-weight-medium">
            Last Name
          </label>
          <input
            type="text"
            placeholder="Enter your Last Name"
            onChange={(e) => onInputChange(e.target.value, "last_name")}
          />
        </div>
        <div className="form-group">
          <label htmlFor="Email" className="mb-2 font-weight-medium">
            Email ID
          </label>
          <input
            type="email"
            placeholder="Enter your Email ID"
            onChange={(e) => onInputChange(e.target.value, "email")}
          />
        </div>
        <div className="form-group">
          <label htmlFor="Phone" className="mb-2 font-weight-medium">
            Phone
          </label>
          <div>
            <input
              type="text"
              placeholder="Enter your new password"
              onChange={(e) => onInputChange(e.target.value, "phone")}
            />
          </div>
        </div>
        <div className="form-group">
          <label htmlFor="Password" className="mb-2 font-weight-medium">
            Password
          </label>
          <div>
            <input
              type={showPassword ? "text" : "password"}
              placeholder="Enter your new password"
              onChange={(e) => onInputChange(e.target.value, "password")}
            />
            <span onClick={onPasswordView}>View</span>
          </div>
        </div>
        <div className="form-group">
        <input
          type="file"
          placeholder="Upload Resume"
          accept=".pdf"
          onChange={onFileChange}
        />
      </div>
       

        <button className="btn btn-primary ">Create Account</button>
      </form>

      <a className="btn btn-secondary my-2" href="/">
        Go Home
      </a>
    </div>
  );
}

export default SignUp;
