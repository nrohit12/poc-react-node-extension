import React, { useEffect, useState } from "react";
import { useParams } from "react-router-dom";
import axiosInstance from "../../_config";

function ResumeMaker() {
  const [generatedResume, setGeneratedResume] = useState(null);
  const [loading, setLoading] = useState(false);
  const params = useParams();

  const fetchFineTuneResume = async () => {
    setLoading(true);

    if (params?.id) {
      if (!loading) {
        const res = await axiosInstance.get(
          `/job/fine-tune-resume/${params.id}`
        );
        if (res.data) {
          setGeneratedResume(res.data.data.generated_response);
        }
      }
    }
    setLoading(false);
  };

  return (
    <div>
      <div>
        {" "}
        {loading ? (
          <h4>Data Loading......</h4>
        ) : (
          <div>
            <h6>Resume Rating: {generatedResume?.rating}</h6>
            <p>Fine Tuned Resume: {generatedResume?.fine_tuned}</p>
            {!generatedResume && (
              <button className="btn btn-primary" onClick={fetchFineTuneResume}>
                Fetch fine tuned Resume
              </button>
            )}
          </div>
        )}
        <a className="btn btn-secondary" href="/">
          Home
        </a>
      </div>
    </div>
  );
}

export default ResumeMaker;
